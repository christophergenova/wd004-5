const todos = [];
//this will serve as the ID counter.
let taskId = 1;

function addTodo(task) {
	// body...
	const
	 newTodo = {
	 	task: task,
	 	id: taskId,
	 	isDone: false
	 }
	 todos.push(newTodo);

	 //once we're done adding the newtodo, we must increment taskId;
	 taskId++;

	 return "Successfully added" + task;
}

function showAllTodos(){
	for( let index = 0; index < todos.length; index++){
		console.log("Task ID:" + todos[index].id + ", Task Name: " + todos[index].task + ", Is Done?: " + todos[index].isDone.toString());
	}
}

function markAsDone(taskId){
	//1. I need to find the task with the corresponding taskID. I need to loop through my current todos arary.
	for (let index = 0; index < todos.length; index++){
		//to find the specific item we want to update, we need to create a condition that will only run if the taskId in the parameter === to the todos[index].id
		if(taskId === todos[index].id){
			todos[index].isDone = true;
		}

	}
	return "Success!";
}