//For loops
//for(starting value decleration; condition; increment/decrement){
	//code block
// }
//
//get the sum of all the numbers from 1-10;
//

// let sum = 0;
// let number = 0;
//while(number <= 10){
	// sum += number;
	// number++
// } 

// for( let number = 0; number <= 10; number++){
// 	sum += number;

// }

// console.log(sum);
// const fruits = ["apple", "banana", "kiwi", "pineapple", "dragon fruit"];
//goal is to console.log each fruit

// for( let index = 0; index < fruits.length; index++){
// 	console.log(fruits[index]);
// }

// .length returns the number of data in an array.
// The last index of an array = array.length - 1
//
//
// get the sum of all even numbers from 1-100;

// let sum=0;


// for(let number = 0; number <= 100; number ++){
// 	//check first if the number is even
// 	if(number % 2 === 0) {
// 		sum += number;

// 	}
// }
// console.log(sum);
//
//task: create a function that accepts a number as a parameter and it will return the sum of all odd numbers less than or equal to the given parameter.
//
// function getOddSum(givenNumber){
// 	let oddSum = 0;
// 	for( let num = 0; num <= givenNumber; num++){
// 		//this checks if num is odd
// 		if(num % 2 !== 0){
// 			oddSum += num;
// 		}
// 	}
// 	return oddSum;
// }

//Create a loop that will console.log the following based on these conditions.
// If the number is divisible by 3 = number + "-Pik"
// If the number is divisible by 5 = number + "-Pak"
// If the number is divisible by 3 AND 5 = number + "PikPakBoom"
// all other numbers = number + "Boom"
//
//
//
//
// let pikgame = 0;

for(let number = 1; number <= 50; number ++){
	//check first if the number is even
	if(number % 3 === 0 && number % 5 === 0) { //pinaka mahigpit ang sa una
		
		console.log(number + " " + "PikPakBoom divisible by 3 & 5");

	}else if(number % 5 === 0) {
		
		console.log(number + " " + "Pak divisible by 5");

	}else if(number % 3 === 0) {
	
		console.log(number + " " + "Pik divisible by 3");
	}else{
		console.log(number + "-Boom out!");
	}

	
};



