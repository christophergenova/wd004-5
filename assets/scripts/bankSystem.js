// Activity:

// bank System.
// List of functionalities.
// 1. addCustomer(name)
// 		customer = {
// 			name: name,
// 			bankAccountNumber: bankAcctNumber,
// 			balance: 0,
// 			isActive: true
// 		}
// 	2. showAllCustomers(){
		//must show all customers individually.
// 	}
// 	
// 	3. freezeCustomer(bankAcctNumber){
// 		//must change the isActive status to false
// 	}
// 	
// 	4. unFreezeCustomer(bankAcctNumber){
// 		//must change the isActive status to true
// 	}
// 	
// 	5. deposit(bankAccountNumber, amount){
// 		//must add the amount to the balance
// 	}
// 	
// 	6. withdraw(bankAccountNumber, amount){
// 		//must deduct the amount
// 	}
// 	// 	7. showAllFrozenCustomer(){
	//will only show customers with isActive === false;
//}
// 8. showAllActiveCustomer(){
// 	//will only show customers with isActive === true;
// }
// To get an S:
// 9. Make sure to validate data. 
// 10. Meaning, you can't withdraw more than your acct. balance
// 11. You can't deposit a negative amount.


const bank = [];
//this will serve as the ID counter.
let bankAcctNumber = 1;

function addCustomer(name) {
	// Adding customer
	const
	 	customer = {
			name: name,
			bankAccountNumber: bankAcctNumber,
			balance: 0,
			isActive: true
	
	 }
	 bank.push(customer);

	 //once we're done adding the newtodo, we must increment taskId;
	 bankAcctNumber++;

	 return "Customer Successfully added" + " " + name;
}

function showAllCustomers(){
	for( let index = 0; index < bank.length; index++){
		console.log("Bank Account Number:" + bank[index].bankAccountNumber + ", Customer Name: " + bank[index].name + ", Bank Balance:" + bank[index].balance + ", Is Active?: " + bank[index].isActive.toString());
	}
}


//must change the isActive status to false.
function freezeCustomer(bankAcctNumber){
		
		for (let index = 0; index < bank.length; index++){
		
		if(bankAcctNumber === bank[index].bankAccountNumber){

			const ifFreeze = bank[index].isActive;
			if(ifFreeze === false){
				return bank[index].name + " " + "is already Freeze"
			}else{
			bank[index].isActive = false;
			}
		}

	}
	
	return "Successfully Freeze account!"; 
	}


function unFreezeCustomer(bankAcctNumber){
		//must change the isActive status to true
		for (let index = 0; index < bank.length; index++){
		
		if(bank[index].bankAccountNumber === bankAcctNumber){
			const ifActive = bank[index].isActive;
			if(ifActive===true){
				return bank[index].name + " " + "is already Active"
			}else{
			bank[index].isActive = true;
			}
		}

	}
	
	return "Successfully Unfreeze account";
}


// DEPOSIT
// To get an S:
// 11. You can't deposit a negative amount.

function deposit(inputID, amount){
// 		//must add the amount to the balance

		for (let index = 0; index < bank.length; index++){
	
		if(inputID === bank[index].bankAccountNumber){

			const checkActiveStat = bank[index].isActive;
			if(amount <= 0){
				return "Please input valid amount"
			}else if(checkActiveStat === false){
				return bank[index].name + " " + "is not active";
			}
			else{
			
			const newBalance = bank[index].balance += amount;
			// bank[index].balance = newBalance; //previous working code
			// console.log("Succesfully added deposit," + " " + "Your new balance is:" + newBalance);
			return "Successfully added Deposit," + " " + bank[index].name +" " + "Your new balance is: " + " " + newBalance;
			}
		}

	}
	// return "Succesfully Added Deposit!!!"; //prev working code LEGEND
	
}



// WITHDRAW FUNCTION
//
//must deduct the amount
// To get an S:

// 10. Meaning, you can't withdraw more than your acct. balance

function withdraw(inputID, amount){
// 		//must add the amount to the balance

		for (let index = 0; index < bank.length; index++){
			
		if(inputID === bank[index].bankAccountNumber){
			
			// const newBalance = bank[index].balance -= amount; //prev working code LEGEND
			const statCheck = bank[index].isActive
			if(statCheck === false){  
				return "You cannot withdraw" + " " + bank[index].name + " " + "is not active"
			}else if((amount > bank[index].balance) || (amount <= 0)){
				return "Insufficient Funds!"
			}
			else{
				const newBalance = bank[index].balance -= amount;
				return "Succesfully Withdraw to account!!!" + bank[index].name + " " + "Your new Balance is:" + " " + newBalance;
			}
			// bank[index].balance = newBalance; //prev working code LEGEND
			// prev: return pos
		}
	}
	// previous place of withdraw

}


// 	7. showAllFrozenCustomer(){


//will only show customers with isActive === false;
function showAllFrozenCustomer(){
	for( let index = 0; index < bank.length; index++){
		const frozenCustomer = bank[index].isActive;
		if( frozenCustomer === false){
			console.log("Bank Account Number:" + bank[index].bankAccountNumber + ", Customer Name: " + bank[index].name + ", Bank Balance:" + bank[index].balance + ", Is Active?: " + bank[index].isActive.toString());
		
		}
	}
}


// 8. showAllActiveCustomer(){
// 	//will only show customers with isActive === true;

function showAllActiveCustomer(){
	for( let index = 0; index < bank.length; index++){
		const unfrozenCustomer = bank[index].isActive;
		if( unfrozenCustomer === true){
		console.log("Bank Account Number:" + bank[index].bankAccountNumber + ", Customer Name: " + bank[index].name + ", Bank Balance:" + bank[index].balance + ", Is Active?: " + bank[index].isActive.toString());

		
		}
	}
}
