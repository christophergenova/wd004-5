//task is to get the sum of all the number from 1-10;

// let sum = 0;

// sum += 1
// sum += 2
// sum += 3
// sum += 4
// sum += 5
// sum += 6
// sum += 7
// sum += 8
// sum += 9
// sum += 10

// console.log(sum);

//3 LOOPS
//
//While(condition){
	// task you want to accomplish
// } 
// it is important to terminate the loop.
//
//
//

// let number = 0;
// let sum = 0;

// while(number < 11) {
// 	//process you want to do
// 	sum += number;
// 	// a way to terminate the condition;
// 	number ++; 

// }

// console.log(sum);

//example 2 
//lets get the sum of all the numbers from 1-1000

let number = 0;
let sum = 0;

// while(number <= 1000){
// 	sum += number;
// 	number++;

// }

// console.log(sum);

//do-while 
//do it at least once, then check the condition.
//
//Make function that console.log("Your age is: ") While age < 5;

let age = 1;
do{
	//task you want to accomplish
	console.log("Your age is:" + age);
	//terminating process
	age ++;

}while(age<5);



